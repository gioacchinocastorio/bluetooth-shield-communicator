import unittest

from business.models import BluetoothBeacon
from utils.remote import RemoteService
from utils.serialization import DeserializerImpl


class MyTestCase(unittest.TestCase):
    def test_something(self):
        serializer = DeserializerImpl()
        message: str = 'OK+DISC:4C000215:6543257465876645324:00060002BF:5AC456D8B5F0:-042'
        result: BluetoothBeacon = serializer.message_from_encoded_string(message)

        remote = RemoteService('https://requestb.in/y7xiycy7')
        remote.handle_message(result)


if __name__ == '__main__':
    unittest.main()
