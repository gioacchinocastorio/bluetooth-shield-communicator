"""
   Copyright 2018 Gioacchino Castorio

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import unittest

from business.models import BluetoothBeacon
from utils.serialization import DeserializerImpl


class BeaconDeserializerTest(unittest.TestCase):
    def test_something(self):
        serializer = DeserializerImpl()

        message: str = 'OK+DISC:4C000215:00047026395836294620463936380484:00060002BF:5AC456D8B5F0:-042'

        result: BluetoothBeacon = serializer.message_from_encoded_string(message)

        self.assertEqual(result.beaconGroupMajor, 6)
        self.assertEqual(result.beaconGroupMinor, 2)
        self.assertEqual(result.bluetoothSSID, '00047026395836294620463936380484')
        self.assertEqual(result.nominalPower, -65)
        self.assertEqual(result.receivedPower, -42)
        self.assertEqual(result.senderMACAddress, '5AC456D8B5F0')


if __name__ == '__main__':
    unittest.main()
