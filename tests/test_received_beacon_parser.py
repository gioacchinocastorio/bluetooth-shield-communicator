"""
   Copyright 2018 Gioacchino Castorio

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import unittest


class MyTestCase(unittest.TestCase):
    def test_something(self):
        single_message = 'OK+DISC:4C000215:00047026395836294620463936380484:00060002BF:5AC456D8B5F0:-042'
        message: str = single_message * 2
        self.assertEqual(self.split_serialized_multibeacon(message), [single_message, single_message])

    def split_serialized_multibeacon(self, multibeacon: str) -> list:
        splitted_serialized_beacons = multibeacon.split("OK+DISC")
        well_formatted_beacons = []
        for serialized_beacon in splitted_serialized_beacons:
            if serialized_beacon is not '':
                well_formatted_beacons.append("OK+DISC" + serialized_beacon)

        return well_formatted_beacons


if __name__ == '__main__':
    unittest.main()
