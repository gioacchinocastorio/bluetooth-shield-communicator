"""
   Copyright 2018 Gioacchino Castorio

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import unittest

import time

from business.models import BluetoothBeacon, SystemLog
from utils.log import FileLogger
from utils.serialization import DeserializerImpl

@unittest.skip
class MyTestCase(unittest.TestCase):
    def test_something(self):
        logger = FileLogger("test.log")
        serializer: DeserializerImpl = DeserializerImpl()
        message: str = 'OK+DISC:4C000215:00047026395836294620463936380484:00060002BF:5AC456D8B5F0:-042'

        result: BluetoothBeacon = serializer.message_from_encoded_string(message)

        logger.handle_message(result)

        sys_connection = SystemLog()
        sys_connection.log = 'connected'
        logger.handle_message(sys_connection)

        sys_connection = time.time()
        logger.handle_message(sys_connection)


if __name__ == '__main__':
    unittest.main()
