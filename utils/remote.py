import requests

from business.dto import BeaconSendBody


class RemoteService(object):

    def __init__(self, url):
        self._url = url
        self._headers = {'Content-type': 'application/json', 'Authorization': 'Updater secret'}

    def handle_message(self, message):
        try:
            status = requests.put(self._url,
                                  data=str(BeaconSendBody(message)),
                                  headers=self._headers)
            print("[INFO] Beacon sent to server")
        except Exception as e:
            print("[ERROR] Server unreachable, beacon discarded")
