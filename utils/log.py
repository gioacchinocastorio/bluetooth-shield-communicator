"""
   Copyright 2018 Gioacchino Castorio

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import logging

from business.models import Message, BluetoothBeacon


class CommandLineLogger(object):

    def handle_message(self, message):
        if isinstance(message, BluetoothBeacon):
            print("\n" + "-" * 30 + "\n")
            print("SSID: " + message.bluetoothSSID)
            print("Terminal MAC: " + message.senderMACAddress)
            print("Actual pwr: " + str(message.receivedPower))
            print("Nominal pwr: " + str(message.nominalPower))
            # TODO vedi dove sta il calcolo della distanza
            print("Shield: " + str(message.receiverUniqueID))
            print("\n" + "-" * 30 + "\n")


class FileLogger(object):

    def __init__(self, filename):
        # configure debugger in file write mode
        print(filename)
        logging.basicConfig(filename=filename, filemode='w', level=logging.INFO)

    def handle_message(self, message):
        if isinstance(message, BluetoothBeacon):
            logging.info("RECEIVED iBeacon: \n" + str(message))
        else:
            logging.warning("UNKNOWN MESSAGE: " + str(message))
