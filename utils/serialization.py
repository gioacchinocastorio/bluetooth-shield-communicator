"""
   Copyright 2018 Gioacchino Castorio

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

from business.models import BluetoothBeacon
from utils.math import twos_complement


class DeserializerImpl:
    """
    Serialized beacon separator char
    """
    _SEPARATOR = ':'

    """
    Received message starter sequence
    """
    _PACKET_STARTER_SEQUENCE = "OK+DISC"

    """
        Received iBeacon starter sequence
    """
    _IBEACON_STARTER_SEQUENCE = "4C000215"

    def message_from_encoded_string(self, serialized):
        field_list = serialized.split(DeserializerImpl._SEPARATOR)
        if field_list[0] == DeserializerImpl._PACKET_STARTER_SEQUENCE and \
                field_list[1] == DeserializerImpl._IBEACON_STARTER_SEQUENCE:
            translated_message = self._beacon_fieldlist_to_model(field_list)
        else:
            raise NotImplementedError('No translation for other messages yet!')
        return translated_message

    def _beacon_fieldlist_to_model(self, field_list):
        translated_beacon = BluetoothBeacon()
        try:
            translated_beacon.bluetoothSSID = field_list[2]
            translated_beacon.beaconGroupMajor = int(field_list[3][:4])
            translated_beacon.beaconGroupMinor = int(field_list[3][5:8])
            translated_beacon.nominalPower = twos_complement(int(field_list[3][8:], 16))
            translated_beacon.senderMACAddress = field_list[4]
            translated_beacon.receivedPower = int(field_list[5])
        except Exception:
            raise IOError('No translation for other messages yet!')
        return translated_beacon
