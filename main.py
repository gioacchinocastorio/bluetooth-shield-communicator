"""
   Copyright 2018 Gioacchino Castorio

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

from business.beacondistance import DistanceCalculatorImpl
from business.logic import Terminal
from business.messagepassing import MessageQueueImpl
from business.serial import SerialParameters, BluetoothSerialReceiverImpl, SerialParity
from utils.remote import RemoteService
from utils.serialization import DeserializerImpl

if __name__ == '__main__':
    # These services can use the messages received from the shields
    consumers = [
        RemoteService('http://localhost:8080/device/beacon')
    ]

    # UTILITIES
    deserializer = DeserializerImpl()
    distance_calculator = DistanceCalculatorImpl()
    message_queue_handler = MessageQueueImpl()

    # SERIAL CONNECTIONS
    # parameters of the first shield
    parameters = SerialParameters()
    # parameters.interface_name = '/dev/tty.bluetoothbasestation'
    parameters.interface_name = '/dev/tty.usbserial'
    parameters.timeout_seconds = 2
    parameters.baud_rate = 9600
    parameters.parity = SerialParity.NONE
    parameters.shieldID = 300

    receivers = [
        BluetoothSerialReceiverImpl(parameters, message_queue_handler, distance_calculator, deserializer)
    ]

    # MAIN SERVICE
    smart_home_terminal = Terminal(receivers, message_queue_handler, consumers)
    smart_home_terminal.run()
