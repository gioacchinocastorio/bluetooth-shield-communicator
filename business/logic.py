"""
   Copyright 2018 Gioacchino Castorio

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""


class Terminal(object):

    def __init__(self, serial_interfaces, message_queuer, message_consuming_services):
        self._serial_interfaces = serial_interfaces
        self._message_queuer = message_queuer
        self._consumer_services = message_consuming_services

    def run(self):
        self._initialise_serial_connections()
        self._start_consuming_messages()

    def _start_consuming_messages(self):
        try:
            while True:
                beacon = self._message_queuer.get_next_message()
                for consumer in self._consumer_services:
                    consumer.handle_message(beacon)
        except KeyboardInterrupt:
            self._stop_listening()

    def _initialise_serial_connections(self):
        for receiver in self._serial_interfaces:
            receiver.initialise_serial_connection()
            receiver.start_async_reading()

    def _stop_listening(self):
        for receiver in self._serial_interfaces:
            receiver.stop_listening()
