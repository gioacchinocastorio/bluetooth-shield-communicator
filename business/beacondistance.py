"""
   Copyright 2018 Gioacchino Castorio

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

from business.models import BluetoothBeacon


class DistanceCalculatorImpl(object):

    def compute_distance_meters(self, beacon: BluetoothBeacon):

        pwr = beacon.nominalPower
        rssi = beacon.receivedPower

        if pwr < 0:
            pl = beacon.nominalPower - beacon.receivedPower

            if pl <= 0:
                distance = self._calculate_distance_gain(pwr, rssi)
            else:
                distance = self._calculate_distance_attenuation(pwr, rssi)
        else:
            distance = -1

        return distance

    def _calculate_distance_gain(self, pwr: int, rssi: int):
        return (rssi / float(pwr)) ** 10

    def _calculate_distance_attenuation(self, pwr: int, rssi: int):
        return 0.89976 * ((rssi / float(pwr)) ** 7.7095) + 0.111
