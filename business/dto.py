import json


class BeaconSendBody(object):

    def __init__(self, message):
        self.bluetoothSSID = message.bluetoothSSID
        self.distanceMeters = message.distanceMeters
        self.shieldSharedID = message.receiverUniqueID

    def __str__(self):
        return json.dumps(self.__dict__, sort_keys=True, indent=4)
