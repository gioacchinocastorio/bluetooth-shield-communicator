"""
   Copyright 2018 Gioacchino Castorio

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

from queue import Queue


class MessageQueueImpl(object):

    def __init__(self):
        super().__init__()
        self._message_queue = Queue()

    def get_next_message(self):
        return self._message_queue.get()

    def enque_message(self, received_message):
        self._message_queue.put(received_message)
