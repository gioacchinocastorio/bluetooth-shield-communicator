"""
   Copyright 2018 Gioacchino Castorio

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import json
from enum import Enum

import serial

from business.models import BluetoothBeacon
from utils import threads


class SerialParity(Enum):
    NONE = 0


class SerialParameters(object):

    def __init__(self):
        self.interface_name = ''
        self.baud_rate = 0
        self.parity = 0
        self.timeout_seconds = 0
        self.shieldID = ''

    def __str__(self):
        return json.dumps(self.__dict__)


class BluetoothSerialReceiverImpl(object):
    """
    Serial message encoding
    """
    ENCODING_UTF8 = "utf-8"

    """
    Received beacon starter sequence
    """
    BEACON_STARTER_SEQUENCE = "OK+DISC"

    """
    Beacon size in bytes
    """
    BEACON_SIZE = 78

    def __init__(self, serial_parameters, beacon_queuer, distance_calulator, deserializer):
        self.serial_parameters = serial_parameters
        self._beacon_queue = beacon_queuer
        self._distance_calculator = distance_calulator
        self._serial = None
        self._deserializer = deserializer
        self._thread = threads.StoppableThread(target=self._read_beacon_thread)

    def stop_listening(self):
        self._thread.stop_thread()

    def initialise_serial_connection(self):
        try:
            self._serial = BluetoothSerialReceiverImpl.get_serial(self.serial_parameters)
            self._bootstrap_bluetooth_shield()
        except ValueError:
            raise IOError('Unable to initialise the serial listener')
        except serial.SerialException:
            raise IOError('Busy serial interface')

    def start_async_reading(self):
        self._thread.start()

    def read_beacon(self):
        beacon = None
        self._serial.reset_input_buffer()
        received_bytes = self._serial.readline(BluetoothSerialReceiverImpl.BEACON_SIZE)  # read beacon size
        received_string = received_bytes.decode(BluetoothSerialReceiverImpl.ENCODING_UTF8)
        if self.BEACON_STARTER_SEQUENCE in received_string:
            serialized_beacons = self._split_concatenated_beacons(received_string)[0]
            try:
                beacon = self._deserializer.message_from_encoded_string(serialized_beacons)
                beacon.distanceMeters = self._distance_calculator.compute_distance_meters(beacon)
                beacon.receiverUniqueID = self.serial_parameters.shieldID
            except Exception:
                beacon = None
        return beacon

    def _read_beacon_thread(self):
        while not self._thread.is_stopped():
            try:
                beacon = self.read_beacon()
                if beacon is not None:
                    self._beacon_queue.enque_message(beacon)
            except Exception as e:
                continue  #  do nothing

    def _send_command_wait_answer(self, command, expected_answer):
        """
        Send a message to the serial receiver and wait for the specified answer.

        :param   command: string command to send
        :param   expected_answer: answer that is supposed to be received within the timeout
        :return: actually received answer
        """

        self._serial.reset_input_buffer()  # Clean serial input buffer

        print("Sent command: " + command)
        print("Expected answer: " + expected_answer)
        self._serial.write(str.encode(command, BluetoothSerialReceiverImpl.ENCODING_UTF8))

        actualanswer = self._serial_read_string_till_expected_answer(expected_answer)
        print("Actual answer: " + actualanswer)

        return actualanswer

    def _serial_read_string_till_expected_answer(self, answer):
        actual_answer = ''
        while True:
            received = self._serial.readline()
            actual_answer += received.decode(BluetoothSerialReceiverImpl.ENCODING_UTF8)
            if answer in actual_answer:
                break
        return actual_answer

    def _split_concatenated_beacons(self, concatenation):
        splitted_serialized_beacons = concatenation.split(self.BEACON_STARTER_SEQUENCE)
        well_formatted_beacons = []
        for serialized_beacon in splitted_serialized_beacons:
            if serialized_beacon != '':  # filters empty elements
                well_formatted_beacons.append(self.BEACON_STARTER_SEQUENCE + serialized_beacon)
        return well_formatted_beacons

    def _bootstrap_bluetooth_shield(self):
        # Reset shield
        self._send_command_wait_answer("AT+RENEW", "OK+RENEW")
        self._send_command_wait_answer("AT+RESET", "OK+RESET")

        # Start beacon reading
        self._send_command_wait_answer("AT+IMME1", "OK+Set:1")
        self._send_command_wait_answer("AT+ROLE1", "OK+Set:1")
        self._send_command_wait_answer("AT+DISI?", "OK+DISCE")
        self._send_command_wait_answer("AT+START", "OK+START")

    @classmethod
    def get_serial(cls, serial_parameters):

        parity = serial.PARITY_NONE
        interface = serial_parameters.interface_name
        baudrate = serial_parameters.baud_rate
        timeout_seconds = serial_parameters.timeout_seconds

        # TODO check for other parity
        if serial_parameters.parity is not SerialParity.NONE:
            raise NotImplementedError("Only parity none for now")

        return serial.Serial(interface,
                             timeout=timeout_seconds,
                             baudrate=baudrate,
                             parity=parity)
