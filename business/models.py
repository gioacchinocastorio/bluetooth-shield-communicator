"""
   Copyright 2018 Gioacchino Castorio

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import json

import time


class Message:
    """
    This class represent every message received from the serial sender
    """

    def __init__(self):
        self.timeStamp = int(time.time())
        self.receiverUniqueID = ''

    def get_message(self):
        return str(self)

    def __str__(self):
        return json.dumps(self.__dict__, sort_keys=True, indent=4)


"""
   Copyright 2018 Gioacchino Castorio

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""


class SystemLog(Message):
    """
    This class represents a Log of the serial sender
    """

    def __init__(self):
        super().__init__()
        self.log = ''

    def get_message(self):
        return "SYSTEM: " + self.log


class BluetoothBeacon(Message):
    """
    This class represents a received Bluetooth Beacon Message
    """

    def __init__(self):
        super().__init__()
        self.beaconGroupMajor = 0
        self.beaconGroupMinor = 0
        self.nominalPower = 0
        self.receivedPower = 0
        self.senderMACAddress = ''
        self.bluetoothSSID = ''
        self.distanceMeters = 0

    def get_message(self):
        return "BEACON: " + str(self)
